from decimal import Decimal

from . import QuadraticBezier, Point


def test_repr():
    curve = QuadraticBezier(Point(10, 10), Point(20, 30), Point(30, 10))
    assert repr(curve) == (
        'QuadraticBezier(Point(10, 10), Point(20, 30), Point(30, 10))')


def test_str():
    curve = QuadraticBezier(Point(10, 10), Point(10, 25), Point(30, 10))
    assert str(curve) == (
        'QuadraticBezier(start=(10, 10), control=(10, 25), end=(30, 10))')


def test_equal():
    curve1 = QuadraticBezier(Point(10, 10), Point(20, 30), Point(30, 10))
    curve2 = QuadraticBezier(Point(10, 10), Point(20, 30), Point(30, 10))
    assert curve1 == curve2


def test_not_equal():
    curve1 = QuadraticBezier(Point(10, 10), Point(20, 30), Point(30, 10))
    curve2 = QuadraticBezier(Point(10, 20), Point(20, 30), Point(30, 10))
    assert curve1 != curve2


def test_bounding_box():
    curve = QuadraticBezier(Point(10, 10), Point(20, 30), Point(30, 10))
    assert curve.bbox() == (Point(10, 10), Point(30, 20))


def test_bounding_box_rounding():
    curve = QuadraticBezier(
        Point(Decimal('9.999999'), 10),
        Point(20, Decimal('30.022489')),
        Point(30, 10)
    )
    assert curve.bbox() == (
        Point(Decimal('9.99999'), 10),
        Point(30, Decimal('20.01125'))
    )
