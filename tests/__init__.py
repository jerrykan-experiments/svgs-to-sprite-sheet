import importlib

script = importlib.import_module('svgs-to-sprite-sheet')

Arc = script.Arc  # type: ignore[attr-defined]
CubicBezier = script.CubicBezier  # type: ignore[attr-defined]
Line = script.Line  # type: ignore[attr-defined]
Path = script.Path  # type: ignore[attr-defined]
Point = script.Point  # type: ignore[attr-defined]
QuadraticBezier = script.QuadraticBezier  # type: ignore[attr-defined]

PathError = script.PathError  # type: ignore[attr-defined]
extract_path = script.extract_path  # type: ignore[attr-defined]
construct_svg_tree = script.construct_svg_tree  # type: ignore[attr-defined]
