from decimal import Decimal

from . import CubicBezier, Point


def test_repr():
    curve = CubicBezier(
        Point(10, 10), Point(10, 30), Point(30, 30), Point(30, 10))
    assert repr(curve) == ('CubicBezier(Point(10, 10), Point(10, 30), '
                           'Point(30, 30), Point(30, 10))')


def test_str():
    curve = CubicBezier(
        Point(10, 10), Point(10, 30), Point(30, 30), Point(30, 10))
    assert str(curve) == ('CubicBezier(start=(10, 10), control1=(10, 30), '
                          'control2=(30, 30), end=(30, 10))')


def test_equal():
    curve1 = CubicBezier(
        Point(10, 10), Point(10, 30), Point(30, 30), Point(30, 10))
    curve2 = CubicBezier(
        Point(10, 10), Point(10, 30), Point(30, 30), Point(30, 10))
    assert curve1 == curve2


def test_not_equal():
    curve1 = CubicBezier(
        Point(10, 10), Point(10, 30), Point(30, 30), Point(30, 10))
    curve2 = CubicBezier(
        Point(10, 20), Point(10, 30), Point(30, 30), Point(30, 10))
    assert curve1 != curve2


def test_bounding_box():
    curve = CubicBezier(
        Point(10, 10), Point(10, 30),
        Point(30, 30), Point(30, 10)
    )
    assert curve.bbox() == (Point(10, 10), Point(30, 25))


def test_bounding_box_rounding():
    curve = CubicBezier(
        Point(Decimal('9.999999'), 10), Point(10, Decimal('30.00105')),
        Point(30, Decimal('30.00104')), Point(30, 10)
    )
    assert curve.bbox() == (Point(Decimal('9.99999'), 10),
                            Point(30, Decimal('25.00079')))
