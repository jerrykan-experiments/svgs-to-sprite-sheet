from . import Arc, CubicBezier, Line, Path, Point, QuadraticBezier

# TODO: invalid path definitions


def test_tokenise_standard():
    items = Path._tokenise('m 100,100 m -50,-50')
    assert items == ['m', '100', '100', 'm', '-50', '-50']


def test_tokenise_compressed():
    items = Path._tokenise('m100-100m-50.0.50m20 50m 100,50M50-0.50m10..10')
    assert items == [
        'm', '100', '-100',
        'm', '-50.0', '.50',
        'm', '20', '50',
        'm', '100', '50',
        'M', '50', '-0.50',
        'm', '10.', '.10',
    ]


def test_tokenise_exponents():
    items = Path._tokenise('m 1.3e-3,-2.4e+2')
    assert items == ['m', '1.3e-3', '-2.4e+2']

    items = Path._tokenise('m -.3e-2,.4e+4')
    assert items == ['m', '-.3e-2', '.4e+4']

    items = Path._tokenise('m -1.50e3,.50e2')
    assert items == ['m', '-1.50e3', '.50e2']


def test_move_absolute():
    path = Path('M 100,100 M 220,175')
    assert path.cur_pos == Point(220, 175)


def test_move_relative():
    path = Path('m 100,100 m -50,-50')
    assert path.cur_pos == Point(50, 50)


def test_line_to_absolute():
    path = Path('M 100,200 L 150,250')
    assert path.segments == [Line(Point(100, 200), Point(150, 250))]
    assert path.cur_pos == Point(150, 250)


def test_line_to_absolute_implied():
    path = Path('M 100,200 250,350')
    assert path.segments == [Line(Point(100, 200), Point(250, 350))]
    assert path.cur_pos == Point(250, 350)


def test_line_to_relative():
    path = Path('M 100,200 l 50,-25')
    assert path.segments == [Line(Point(100, 200), Point(150, 175))]
    assert path.cur_pos == Point(150, 175)


def test_line_to_relative_implied():
    path = Path('m 100,200 100,-50')
    assert path.segments == [Line(Point(100, 200), Point(200, 150))]
    assert path.cur_pos == Point(200, 150)


def test_line_horizontal_absolute():
    path = Path('M 100,100 H 150')
    assert path.segments == [Line(Point(100, 100), Point(150, 100))]
    assert path.cur_pos == Point(150, 100)


def test_line_horizontal_relative():
    path = Path('M 100,100 h 150')
    assert path.segments == [Line(Point(100, 100), Point(250, 100))]
    assert path.cur_pos == Point(250, 100)


def test_line_vertical_absolute():
    path = Path('M 100,100 V 300')
    assert path.segments == [Line(Point(100, 100), Point(100, 300))]
    assert path.cur_pos == Point(100, 300)


def test_line_vertical_relative():
    path = Path('M 100,100 v 150')
    assert path.segments == [Line(Point(100, 100), Point(100, 250))]
    assert path.cur_pos == Point(100, 250)


def test_cubic_bezier_absolute():
    path = Path('M 100,100 C 100,300 300,300 300,100')
    assert path.segments == [CubicBezier(
        Point(100, 100),
        Point(100, 300),
        Point(300, 300),
        Point(300, 100)
    )]
    assert path.cur_pos == Point(300, 100)


def test_cubic_bezier_relative():
    path = Path('M 100,100 c 10,100 90,150 100,10')
    assert path.segments == [CubicBezier(
        Point(100, 100),
        Point(110, 200),
        Point(190, 250),
        Point(200, 110)
    )]
    assert path.cur_pos == Point(200, 110)


def test_cubic_bezier_smooth_absolute():
    path = Path('M 100,100 C 100,150 180,200 200,100 S 300,0 300,50')
    assert path.segments == [
        CubicBezier(
            Point(100, 100),
            Point(100, 150),
            Point(180, 200),
            Point(200, 100)
        ),
        CubicBezier(
            Point(200, 100),
            Point(220, 0),
            Point(300, 0),
            Point(300, 50)
        ),
    ]
    assert path.cur_pos == Point(300, 50)


def test_cubic_bezier_smooth_absolute_with_no_previous_cubic_bezier():
    path = Path('M 100,200 V 100 S 300,0 300,50')
    assert path.segments == [
        Line(Point(100, 200), Point(100, 100)),
        CubicBezier(
            Point(100, 100),
            Point(100, 100),
            Point(300, 0),
            Point(300, 50)
        ),
    ]
    assert path.cur_pos == Point(300, 50)


def test_cubic_bezier_smooth_absolute_with_no_previous_segment():
    path = Path('M 100,200 S 300,0 300,50')
    assert path.segments == [CubicBezier(
        Point(100, 200),
        Point(100, 200),
        Point(300, 0),
        Point(300, 50)
    )]
    assert path.cur_pos == Point(300, 50)


def test_cubic_bezier_smooth_relative():
    path = Path('M 100,100 C 100,150 160,180 190,120 s 50,50 100,50')
    assert path.segments == [
        CubicBezier(
            Point(100, 100),
            Point(100, 150),
            Point(160, 180),
            Point(190, 120)
        ),
        CubicBezier(
            Point(190, 120),
            Point(220, 60),
            Point(240, 170),
            Point(290, 170)
        ),
    ]
    assert path.cur_pos == Point(290, 170)


def test_cubic_bezier_smooth_relative_with_no_previous_cubic_bezier():
    path = Path('M 100,125 V 75 s 100,50 150,-50')
    assert path.segments == [
        Line(Point(100, 125), Point(100, 75)),
        CubicBezier(
            Point(100, 75),
            Point(100, 75),
            Point(200, 125),
            Point(250, 25)
        ),
    ]
    assert path.cur_pos == Point(250, 25)


def test_cubic_bezier_smooth_relative_with_no_previous_segment():
    path = Path('M 150,150 s 50,-50 100,50')
    assert path.segments == [CubicBezier(
        Point(150, 150),
        Point(150, 150),
        Point(200, 100),
        Point(250, 200)
    )]
    assert path.cur_pos == Point(250, 200)


def test_quadratic_bezier_absolute():
    path = Path('M 100,100 Q 150,200 250,175')
    assert path.segments == [
        QuadraticBezier(Point(100, 100), Point(150, 200), Point(250, 175))
    ]
    assert path.cur_pos == Point(250, 175)


def test_quadratic_bezier_relative():
    path = Path('M 100,125 q 75,125 100,50')
    assert path.segments == [
        QuadraticBezier(Point(100, 125), Point(175, 250), Point(200, 175))
    ]
    assert path.cur_pos == Point(200, 175)


def test_quadratic_bezier_smooth_absolute():
    path = Path('M 50,50 Q 100,150 125,125 T 200,125')
    assert path.segments == [
        QuadraticBezier(Point(50, 50), Point(100, 150), Point(125, 125)),
        QuadraticBezier(Point(125, 125), Point(150, 100), Point(200, 125))
    ]
    assert path.cur_pos == Point(200, 125)


def test_quadratic_bezier_smooth_absolute_with_no_previous_quadratic_bezier():
    path = Path('M 50,75 H 100 T 200,125')
    assert path.segments == [
        Line(Point(50, 75), Point(100, 75)),
        QuadraticBezier(Point(100, 75), Point(100, 75), Point(200, 125))
    ]
    assert path.cur_pos == Point(200, 125)


def test_quadratic_bezier_smooth_absolute_with_no_previous_segment():
    path = Path('M 50,75 T 100,125')
    assert path.segments == [
        QuadraticBezier(Point(50, 75), Point(50, 75), Point(100, 125))
    ]
    assert path.cur_pos == Point(100, 125)


def test_quadratic_bezier_smooth_relative():
    path = Path('M 75,50 Q 100,150 150,125 t 75,75')
    assert path.segments == [
        QuadraticBezier(Point(75, 50), Point(100, 150), Point(150, 125)),
        QuadraticBezier(Point(150, 125), Point(200, 100), Point(225, 200))
    ]
    assert path.cur_pos == Point(225, 200)


def test_quadratic_bezier_smooth_relative_with_no_previous_quadratic_bezier():
    path = Path('M 75,50 V 100 t 100,75')
    assert path.segments == [
        Line(Point(75, 50), Point(75, 100)),
        QuadraticBezier(Point(75, 100), Point(75, 100), Point(175, 175))
    ]
    assert path.cur_pos == Point(175, 175)


def test_quadratic_bezier_smooth_relative_with_no_previous_segment():
    path = Path('M 75,50 t 75,100')
    assert path.segments == [
        QuadraticBezier(Point(75, 50), Point(75, 50), Point(150, 150))
    ]
    assert path.cur_pos == Point(150, 150)


def test_elliptical_arc_absolute():
    path = Path('M 50,50 A 60,30 90 1 0 150,50')
    assert path.segments == [
        Arc(Point(50, 50), Point(60, 30), 90, True, False, Point(150, 50))
    ]


def test_elliptical_arc_relative():
    path = Path('M 50,50 a 60,30 90 1 0 150,50')
    assert path.segments == [
        Arc(Point(50, 50), Point(60, 30), 90, True, False, Point(200, 100))
    ]


def test_close_path_lowercase():
    path = Path('M 100,100 h 150 v 100 z')
    assert path.segments == [
        Line(Point(100, 100), Point(250, 100)),
        Line(Point(250, 100), Point(250, 200)),
        Line(Point(250, 200), Point(100, 100)),
    ]


def test_close_path_uppercase():
    path = Path('M 100,100 h 150 v 100 Z')
    assert path.segments == [
        Line(Point(100, 100), Point(250, 100)),
        Line(Point(250, 100), Point(250, 200)),
        Line(Point(250, 200), Point(100, 100)),
    ]


def test_close_path_multiple_closes():
    path = Path('M 100,100 h 150 v 100 Z M 150,150 h 100 z')
    assert path.segments == [
        Line(Point(100, 100), Point(250, 100)),
        Line(Point(250, 100), Point(250, 200)),
        Line(Point(250, 200), Point(100, 100)),
        Line(Point(150, 150), Point(250, 150)),
        Line(Point(250, 150), Point(150, 150)),
    ]


def test_bounding_box():
    path = Path('M 100,100 L 200,200 L 50,300 L 250,50 L 130,50')
    assert path.bbox() == (Point(50, 50), Point(250, 300))
