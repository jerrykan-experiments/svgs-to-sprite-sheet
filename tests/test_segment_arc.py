from decimal import Decimal

from . import Arc, Point


def test_repr():
    arc = Arc(Point(100, 100), Point(20, 40), 30, True, False, Point(150, 150))
    assert repr(arc) == (
        'Arc(Point(100, 100), Point(20, 40), 30, True, False, Point(150, 150))'
    )


def test_str():
    arc = Arc(Point(150, 100), Point(30, 45), 60, False, True, Point(200, 200))
    assert str(arc) == (
        'Arc(start=(150, 100), radius=(30, 45), angle=60, large_arc=False, '
        + 'sweep=True, end=(200, 200))'
    )


def test_equal():
    arc1 = Arc(Point(125, 125), Point(60, 30), 120, True, True,
               Point(200, 225))
    arc2 = Arc(Point(125, 125), Point(60, 30), 120, True, True,
               Point(200, 225))
    assert arc1 == arc2


def test_not_equal():
    arc1 = Arc(Point(125, 125), Point(60, 30), 120, True, True,
               Point(200, 225))
    arc2 = Arc(Point(125, 125), Point(60, 30), 60, True, True,
               Point(200, 225))
    assert arc1 != arc2


def test_bounding_box_180_arc_down():
    line = Arc(Point(50, 50), Point(60, 30), 90, True, False, Point(150, 50))
    assert line.bbox() == (Point(50, 50), Point(150, 150))


def test_bounding_box_180_arc_up():
    line = Arc(Point(50, 50), Point(60, 30), 90, False, True, Point(150, 50))
    assert line.bbox() == (Point(50, -50), Point(150, 50))


def test_bounding_box_180_arc_right():
    line = Arc(Point(50, 50), Point(50, 70), 90, False, True, Point(50, 150))
    assert line.bbox() == (Point(50, 50), Point(120, 150))


def test_bounding_box_no_rotation():
    line = Arc(Point(50, 50), Point(60, 30), 0, True, False, Point(150, 50))
    assert line.bbox() == (Point(40, 50), Point(160, '96.58313'))


def test_bounding_box_rotated():
    line = Arc(Point(50, 50), Point(60, 30), 135, True, False, Point(150, 50))
    assert line.bbox() == (Point(37.5, 50), Point(150, 112.5))


def test_bounding_box_small_arc():
    line = Arc(Point(50, 50), Point(60, 30), 30, False, False, Point(120, 60))
    assert line.bbox() == (Point(50, 50), Point(120, Decimal('66.85392')))


def test_bounding_box_small_arc_sweep():
    line = Arc(Point(50, 50), Point(60, 30), 30, False, True, Point(120, 60))
    assert line.bbox() == (Point(50, Decimal('43.14608')), Point(120, 60))


def test_bounding_box_large_arc():
    line = Arc(Point(50, 50), Point(60, 30), 30, True, False, Point(120, 60))
    assert line.bbox() == (
        Point(Decimal('45.76765'), 50),
        Point(Decimal('153.93419'), Decimal('122.51863'))
    )


def test_bounding_box_large_arc_sweep():
    line = Arc(Point(50, 50), Point(60, 30), 30, True, True, Point(120, 60))
    assert line.bbox() == (
        Point(Decimal('16.06581'), Decimal('-12.51863')),
        Point(Decimal('124.23235'), 60)
    )


def test_bounding_box_samepoint():
    line = Arc(Point(50, 50), Point(60, 30), 135, True, False, Point(50, 50))
    assert line.bbox() == (Point(50, 50), Point(50, 50))


def test_bounding_box_no_radius():
    line = Arc(Point(250, 50), Point(0, 0), 135, True, False, Point(150, 100))
    assert line.bbox() == (Point(150, 50), Point(250, 100))
