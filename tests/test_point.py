from . import Point


# TODO: test values that can't be cast to Decimals

def test_repr():
    assert repr(Point(1, 2)) == 'Point(1, 2)'


def test_str():
    assert str(Point(1, 2)) == '(1, 2)'


# TODO: equality with non-Point objects
def test_equal():
    assert Point(1, 2) == Point(1, 2)


def test_not_equal():
    assert Point(1, 2) != Point(2, 1)


# TODO: addition with non-Point objects
def test_add():
    point = Point(1, 3) + Point(3, 10)
    assert point.x == 4
    assert point.y == 13


def test_reflection():
    point = Point(10, 15)
    assert point.reflect(Point(8, 19)) == Point(12, 11)
