from xml.etree import ElementTree

import pytest

from . import PathError, Point, construct_svg_tree, extract_path


def test_extract_path_missing_path():
    svg_dom = ElementTree.fromstring(
        '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 400 400">'
        + '</svg>'
    )
    with pytest.raises(PathError) as err:
        extract_path(svg_dom)

    assert err.match("No 'path' element found in document")


def test_extract_path_single_path():
    svg_dom = ElementTree.fromstring(
        '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 400 400">'
        + '<path d="M 50,50 L 150,50" style="fill: green" />'
        + '</svg>'
    )
    path = extract_path(svg_dom)
    assert path == {
        'd': 'M 50,50 L 150,50',
        'bbox': (Point(50, 50), Point(150, 50)),
    }


def test_extract_path_single_path_deep():
    svg_dom = ElementTree.fromstring(
        '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 400 400">'
        + '<g><g>'
        + '<path d="M 50,50 L 150,50" style="fill: green" />'
        + '</g></g>'
        + '</svg>'
    )
    path = extract_path(svg_dom)
    assert path == {
        'd': 'M 50,50 L 150,50',
        'bbox': (Point(50, 50), Point(150, 50)),
    }


def test_extract_path_missing_namespace():
    svg_dom = ElementTree.fromstring(
        '<svg>'
        + '<path d="M 50,50 L 150,50" style="fill: green" />'
        + '</svg>'
    )
    path = extract_path(svg_dom)
    assert path == {
        'd': 'M 50,50 L 150,50',
        'bbox': (Point(50, 50), Point(150, 50)),
    }


def test_extract_path_from_multiple_paths():
    svg_dom = ElementTree.fromstring(
        '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 400 400">'
        + '<path d="M 50,50 L 150,50" style="fill: green" />'
        + '<path d="M 150,150 L 150,50" style="fill: green" />'
        + '</svg>'
    )
    path = extract_path(svg_dom)
    assert path == {
        'd': 'M 50,50 L 150,50',
        'bbox': (Point(50, 50), Point(150, 50)),
    }


def test_construct_svg_tree_no_previews():
    paths = [{
        'd': 'M 0,0 L 100,100 L 100,0',
        'bbox': (Point(0, 0), Point(100, 100)),
        'name': 'path1',
    }, {
        'd': 'M 100,100 L 200,200 L 200,100',
        'bbox': (Point(100, 100), Point(200, 200)),
        'name': 'path2',
    }]

    svg = construct_svg_tree(paths, previews=False)

    assert ElementTree.tostring(svg).decode() == (
        '<svg xmlns="http://www.w3.org/2000/svg">'
        + '<symbol id="path1" viewBox="0 0 100 100">'
        + '<path d="M 0,0 L 100,100 L 100,0" />'
        + '</symbol>'
        + '<symbol id="path2" viewBox="100 100 100 100">'
        + '<path d="M 100,100 L 200,200 L 200,100" />'
        + '</symbol>'
        + '</svg>'
    )


def test_construct_svg_tree_previews():
    paths = [{
        'd': 'M 0,0 L 100,100 L 100,0',
        'bbox': (Point(0, 0), Point(100, 100)),
        'name': 'path1',
    }, {
        'd': 'M 100,100 L 200,200 L 200,100',
        'bbox': (Point(100, 100), Point(200, 200)),
        'name': 'path2',
    }]

    svg = construct_svg_tree(paths, previews=True)

    assert ElementTree.tostring(svg).decode() == (
        '<svg xmlns="http://www.w3.org/2000/svg"'
        + ' xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 200 200">'
        + '<symbol id="path1" viewBox="0 0 100 100">'
        + '<path d="M 0,0 L 100,100 L 100,0" />'
        + '</symbol>'
        + '<symbol id="path2" viewBox="100 100 100 100">'
        + '<path d="M 100,100 L 200,200 L 200,100" />'
        + '</symbol>'
        + '<use xlink:href="#path1" x="0" y="0" width="100" height="100" />'
        + '<use xlink:href="#path2" x="100" y="0" width="100" height="100" />'
        + '</svg>'
    )
