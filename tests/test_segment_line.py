from decimal import Decimal

from . import Line, Point


def test_repr():
    line = Line(Point(10, 10), Point(20, 20))
    assert repr(line) == 'Line(Point(10, 10), Point(20, 20))'


def test_str():
    line = Line(Point(10, 10), Point(20, 20))
    assert str(line) == 'Line(start=(10, 10), end=(20, 20))'


# TODO: equality with non-Line objects
def test_equal():
    line1 = Line(Point(20, 25), Point(30, 40))
    line2 = Line(Point(20, 25), Point(30, 40))
    assert line1 == line2


def test_not_equal():
    line1 = Line(Point(20, 25), Point(30, 40))
    line2 = Line(Point(25, 25), Point(30, 40))
    assert line1 != line2


def test_bounding_box():
    line = Line(Point(40, 25), Point(30, 40))
    assert line.bbox() == (Point(30, 25), Point(40, 40))


def test_bounding_box_rounding():
    # if rounding to nearest 5th decimal, ensure bounding box is slightly
    # too large instead of slight too small
    line = Line(
        Point(Decimal('-40.123451'), Decimal('25.432516')),
        Point(Decimal('30.543211'), Decimal('40.341265'))
    )
    assert line.bbox() == (
        Point(Decimal('-40.12346'), Decimal('25.43251')),
        Point(Decimal('30.54322'), Decimal('40.34127'))
    )
