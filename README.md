# Prototype: SVG Sprite Sheet Tool
A simple prototype tool for creating [SVG "sprite
sheets"](https://jerrykan-experiments.gitlab.io/web-svg-icons/) from multiple
SVG documents.

This is intended to build on previous experiments:
  - [inkscape-to-sprites](https://gitlab.com/jerrykan-experiments/inkscape-to-sprites)
  - [svg-path-bounding-box](https://gitlab.com/jerrykan-experiments/svg-path-bounding-box)

The `inkscape-to-sprites` experiment was limit by a number of assumptions
that needed to be made about the SVG files the "sprite sheet" was being
generated from.

The `svg-path-bounding-box` experiment was an incomplete implementation of the
SVG path spec (specifically it did not implement elliptical arcs).

The intent of this prototype is to fully implement calculating the bounding box
of all SVG path commands. By knowing the bounding box of a path we could remove
most of the assumptions required by the initial `inkscape-to-sprites` tool.

## Goals
  - The tool should be a single script with no external dependencies
  - Experiment with Python type hinting

## Assumptions
  - The input SVG files should contain a single `<path>` element. If multiple
    `<path>` elements exist, only the first one will be used.
  - Each input SVG file should have a unique
    [basename](https://en.wikipedia.org/wiki/Basename).

## Design Decisions
The decision to store `Point` values as `Decimals` was based on the desire to
have bounding box values be as closely accurate to the original values in the
SVG files, without introducing rounding errors caused by float values.

## Creating a "Sprite Sheet"

    $ ./svgs-to-sprite-sheet.py -p *.svg > sprite_sheet.svg

## Running the Tests/Checks

    $ python3 -m venv venv
    $ venv/bin/pip install -r requirements.txt
    $ venv/bin/python -m pytest tests/
    $ venv/bin/flake8 svgs-to-sprite-sheet.py tests/*.py
    $ venv/bin/mypy svgs-to-sprite-sheet.py tests/*.py

## References
  - [SVG `d` attribute](https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/d)
  - [Cubic bezier curve bounding box algorithm](https://stackoverflow.com/questions/2587751/an-algorithm-to-find-bounding-box-of-closed-bezier-curves/14429749#14429749) - implementation based on "Code 1" section.
  - [Quadratic bezier curve bounding box algorithm](https://stackoverflow.com/questions/2587751/an-algorithm-to-find-bounding-box-of-closed-bezier-curves/2587895#2587895) - algorithm calculations based on comments
  - [SVG2 Specification - Appendix B: Implementation Notes](https://www.w3.org/TR/SVG/implnote.html#ArcImplementationNotes)
  - [Bounding Box of an SVG Elliptical Arc ](https://fridrich.blogspot.com/2011/06/bounding-box-of-svg-elliptical-arc.html)
