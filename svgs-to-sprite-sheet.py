#!/usr/bin/env python3
from __future__ import annotations

import math
import pathlib
import re
from decimal import Decimal
from typing import List, Sequence, Text, Tuple, Union
from xml.etree import ElementTree

# https://github.com/python/typeshed/blob/5be9c915181ffb3e12d61ce0d740098cc9dfcbd1/stdlib/2and3/decimal.pyi#L7
T_DecimalNew = Union[Decimal, float, Text, Tuple[int, Sequence[int], int]]


class Point:
    def __init__(self, x: T_DecimalNew, y: T_DecimalNew) -> None:
        self.x = Decimal(x)
        self.y = Decimal(y)

    def __str__(self) -> str:
        return '({0.x}, {0.y})'.format(self)

    def __repr__(self) -> str:
        return 'Point{}'.format(str(self))

    def __eq__(self, other) -> bool:
        return self.x == other.x and self.y == other.y

    def __add__(self, other) -> Point:
        return Point(self.x + other.x, self.y + other.y)

    def __sub__(self, other) -> Point:
        return Point(self.x - other.x, self.y - other.y)

    def reflect(self, point: Point) -> Point:
        return Point(
            self.x + (self.x - point.x),
            self.y + (self.y - point.y),
        )


def _decimal_ceil(value: T_DecimalNew, decimals: int = 5) -> Decimal:
    if isinstance(value, float):
        value = round(Decimal(value), decimals + 1)
    return Decimal(math.ceil(Decimal(value) * 10**decimals)) / 10**decimals


def _decimal_floor(value: T_DecimalNew, decimals: int = 5) -> Decimal:
    if isinstance(value, float):
        value = round(Decimal(value), decimals + 1)
    return Decimal(math.floor(Decimal(value) * 10**decimals)) / 10**decimals


class Line:
    def __init__(self, start: Point, end: Point) -> None:
        self.start = start
        self.end = end

    def __str__(self) -> str:
        return 'Line(start={}, end={})'.format(self.start, self.end)

    def __repr__(self) -> str:
        return 'Line({}, {})'.format(repr(self.start), repr(self.end))

    def __eq__(self, other) -> bool:
        return self.start == other.start and self.end == other.end

    def bbox(self) -> Tuple[Point, Point]:
        return (
            Point(
                _decimal_floor(min(self.start.x, self.end.x)),
                _decimal_floor(min(self.start.y, self.end.y)),
            ),
            Point(
                _decimal_ceil(max(self.start.x, self.end.x)),
                _decimal_ceil(max(self.start.y, self.end.y)),
            ),
        )


class CubicBezier:
    def __init__(self, start: Point, control1: Point,
                 control2: Point, end: Point):
        self.start = start
        self.control1 = control1
        self.control2 = control2
        self.end = end

    def __str__(self) -> str:
        return (
            'CubicBezier(start={}, control1={}, control2={}, end={})'
        ).format(self.start, self.control1, self.control2, self.end)

    def __repr__(self) -> str:
        return 'CubicBezier({}, {}, {}, {})'.format(
            repr(self.start), repr(self.control1), repr(self.control2),
            repr(self.end))

    def __eq__(self, other) -> bool:
        return (
            self.start == other.start
            and self.control1 == other.control1
            and self.control2 == other.control2
            and self.end == other.end
        )

    def bbox(self) -> Tuple[Point, Point]:
        # ref: Based on algorithm from https://stackoverflow.com/a/14429749
        #   specifically "Code 1"
        tvalues = []

        for axis in ['x', 'y']:
            a = (
                (-3 * getattr(self.start, axis))
                + (9 * getattr(self.control1, axis))
                - (9 * getattr(self.control2, axis))
                + (3 * getattr(self.end, axis))
            )
            b = (
                (6 * getattr(self.start, axis))
                - (12 * getattr(self.control1, axis))
                + (6 * getattr(self.control2, axis))
            )
            c = (
                (3 * getattr(self.control1, axis))
                - (3 * getattr(self.start, axis))
            )

            if a == 0:
                if b == 0:
                    continue

                t = -c / b
                if 0 < t < 1:
                    tvalues.append(t)

                continue

            # b^2 - 4ac
            b2ac = (b * b) - (4 * c * a)
            if b2ac < 0:
                continue

            sqrtb2ac = Decimal(math.sqrt(b2ac))

            t1 = (-b + sqrtb2ac) / (2 * a)
            if 0 < t1 < 1:
                tvalues.append(t1)

            t2 = (-b - sqrtb2ac) / (2 * a)
            if 0 < t2 < 1:
                tvalues.append(t2)

        values = {
            'x': [self.start.x, self.end.x],
            'y': [self.start.y, self.end.y],
        }

        for t in tvalues:
            mt = 1 - t
            for axis in ['x', 'y']:
                values[axis].append(
                    (mt * mt * mt * getattr(self.start, axis))
                    + (3 * mt * mt * t * getattr(self.control1, axis))
                    + (3 * mt * t * t * getattr(self.control2, axis))
                    + (t * t * t * getattr(self.end, axis))
                )

        return (
            Point(
                _decimal_floor(min(values['x'])),
                _decimal_floor(min(values['y'])),
            ),
            Point(
                _decimal_ceil(max(values['x'])),
                _decimal_ceil(max(values['y'])),
            ),
        )


class QuadraticBezier:
    def __init__(self, start: Point, control: Point, end: Point):
        self.start = start
        self.control = control
        self.end = end

    def __str__(self) -> str:
        return 'QuadraticBezier(start={}, control={}, end={})'.format(
            self.start, self.control, self.end)

    def __repr__(self) -> str:
        return 'QuadraticBezier({}, {}, {})'.format(
            repr(self.start), repr(self.control), repr(self.end))

    def __eq__(self, other) -> bool:
        return (
            self.start == other.start
            and self.control == other.control
            and self.end == other.end
        )

    def bbox(self) -> Tuple[Point, Point]:
        # ref: based on information from https://stackoverflow.com/a/2587895
        values = {
            'x': [self.start.x, self.end.x],
            'y': [self.start.y, self.end.y],
        }

        for axis in ['x', 'y']:
            a = (
                - getattr(self.start, axis)
                + (2 * getattr(self.control, axis))
                - getattr(self.end, axis)
            )
            b = getattr(self.start, axis) - getattr(self.control, axis)

            if a == 0:
                continue

            t = -b / a

            if 0 < t < 1:
                mt = 1 - t
                values[axis].append(
                    (mt * mt * getattr(self.start, axis))
                    + (2 * mt * t * getattr(self.control, axis))
                    + (t * t * getattr(self.end, axis))
                )

        return (
            Point(
                _decimal_floor(min(values['x'])),
                _decimal_floor(min(values['y'])),
            ),
            Point(
                _decimal_ceil(max(values['x'])),
                _decimal_ceil(max(values['y'])),
            ),
        )


class Arc:
    def __init__(self,
                 start: Point,
                 radius: Point,
                 angle: T_DecimalNew,
                 large_arc: bool,
                 sweep: bool,
                 end: Point):
        self.start = start
        self.radius = radius
        self.angle = Decimal(angle)
        self.large_arc = large_arc
        self.sweep = sweep
        self.end = end

    def __str__(self) -> str:
        return (
            ('Arc(start={}, radius={}, angle={}, large_arc={}, '
             + 'sweep={}, end={})').format(
                self.start, self.radius, self.angle, self.large_arc,
                self.sweep, self.end)
        )

    def __repr__(self) -> str:
        return 'Arc({}, {}, {}, {}, {}, {})'.format(
            repr(self.start), repr(self.radius), self.angle, self.large_arc,
            self.sweep, repr(self.end)
        )

    def __eq__(self, other) -> bool:
        return (
            self.start == other.start
            and self.radius == other.radius
            and self.angle == other.angle
            and self.large_arc == other.large_arc
            and self.sweep == other.sweep
            and self.end == other.end
        )

    def bbox(self) -> Tuple[Point, Point]:
        # ref: First part based on formulas in the spec appendix:
        #       https://www.w3.org/TR/SVG/implnote.html#ArcImplementationNotes
        #   and this C implementation:
        #       https://fridrich.blogspot.com/2011/06/bounding-box-of-svg-elliptical-arc.html
        if self.start == self.end:
            return (self.start, self.end)

        # An elipse of no radius is a line
        if self.radius.x == 0 or self.radius.y == 0:
            return Line(self.start, self.end).bbox()

        rx = float(abs(self.radius.x))
        ry = float(abs(self.radius.y))

        phi = math.radians(self.angle)
        sinp = math.sin(phi)
        cosp = math.cos(phi)

        # Compute (x1', y1') - transformed start point
        x_mid = float(self.start.x - self.end.x) / 2
        y_mid = float(self.start.y - self.end.y) / 2
        x1p = (cosp * x_mid) + (sinp * y_mid)
        y1p = (-sinp * x_mid) + (cosp * y_mid)

        # Ensure radii are large enough
        lambda_ = (x1p**2 / rx**2) + (y1p**2 / ry**2)
        if lambda_ > 1:
            l_factor = math.sqrt(lambda_)
            rx *= l_factor
            ry *= l_factor

        # Compute (cx', cy') - transformed/local centre
        try:
            c_factor = math.sqrt(
                ((rx**2 * ry**2) - (rx**2 * y1p**2) - (ry**2 * x1p**2))
                / ((rx**2 * y1p**2) + (ry**2 * x1p**2))
            )

            if self.large_arc == self.sweep:
                c_factor = -c_factor

            cxp = c_factor * (rx * y1p / ry)
            cyp = c_factor * -(ry * x1p / rx)
        except ValueError:
            # Can't find sqrt of negative number (caused by rounding errors?)
            # use workaround from C implementation referenced in comments above
            ratio = rx / ry
            ry = math.sqrt(y1p**2 + (x1p**2 / ratio**2))
            rx = ry * ratio
            cxp = 0.0
            cyp = 0.0

        # Compute (cx, cy) from (cx', cy') - original/global centre
        centre = Point(
            (cosp * cxp - sinp * cyp) + float((self.start.x + self.end.x) / 2),
            (sinp * cxp + cosp * cyp) + float((self.start.y + self.end.y) / 2),
        )

        # Begin/End angle (clockwise is +ve because y-axis is inverted)
        t_start = math.atan2(rx * (y1p - cyp), ry * (x1p - cxp))
        t_end = math.atan2(rx * (-y1p - cyp), ry * (-x1p - cxp))
        tau = math.pi * 2

        # Adjust Begin/End angle in [0, 2*tau)
        t_start %= tau
        t_end %= tau

        if t_start > t_end:
            t_end += tau

        # Determine which arc is being used
        delta = t_end - t_start

        if self.large_arc and delta <= math.pi:
            t_start = t_end % tau
            t_end = t_start + (tau - delta)
        elif not self.large_arc and delta > math.pi:
            t_start = t_end % tau
            t_end = t_start + (tau - delta)

        # Calculate the extremes of each axis within this arc
        tanp = math.tan(phi)
        if tanp == 0:
            # HACK: ensure tanp is never exactly 0
            tanp += 2.2250738585072014e-308
        extrema = (Point(0, 0), Point(0, 0))

        for axis, theta, tx, ty in (
            ('x', -math.atan((ry * tanp) / rx), cosp, -sinp),
            ('y', math.atan(ry / (tanp * rx)), sinp, cosp),
        ):
            candidates = [t_start, t_end]
            while theta < t_end:
                if t_start < theta:
                    candidates.append(theta)
                theta += math.pi

            values = []
            for theta in candidates:
                values.append(
                    float(getattr(centre, axis))
                    + (rx * tx * math.cos(theta))
                    + (ry * ty * math.sin(theta))
                )

            setattr(extrema[0], axis, _decimal_floor(min(values)))
            setattr(extrema[1], axis, _decimal_ceil(max(values)))

        return extrema


T_Segment = Union[Arc, CubicBezier, Line, QuadraticBezier]
T_Segments = List[T_Segment]


class Path:
    commands = [
        'M', 'm',  # Move To
        'L', 'l', 'H', 'h', 'V', 'v',  # Line To
        'C', 'c', 'S', 's',  # Cubic Bezier Curse
        'Q', 'q', 'T', 't',  # Quadratic Bezier Curve
        'A', 'a',  # Elliptical Arc Curve
        'Z', 'z',  # Close Path
    ]

    def __init__(self, d: str) -> None:
        self.cur_pos = Point(0, 0)
        self.segments: T_Segments = []

        self._parse(d)

    @classmethod
    def _tokenise(cls, string: str) -> List[str]:
        regex = '({})'.format(
            '|'.join([
                '[{}]'.format(''.join(cls.commands)),
                r'-?[0-9]+(\.[0-9]*)?(e[+-]?[0-9]+)?',
                r'-?\.[0-9]+(e[+-]?[0-9]+)?',
            ])
        )
        items = []

        string = string.strip()
        while string:
            match = re.match(regex, string)
            if match:
                items.append(string[:match.end()])
                string = string[match.end():].lstrip(' ,')
            else:
                raise Exception('lexer error')

        return items

    def _parse(self, d: str) -> None:
        directions = self._tokenise(d)
        segment: T_Segment
        self.last_move = Point(0, 0)

        while directions:
            if directions[0] in self.commands:
                cmd = directions.pop(0)
            else:
                if cmd == 'M':
                    cmd = 'L'
                elif cmd == 'm':
                    cmd = 'l'

            if cmd == 'M':
                self.cur_pos = Point(directions.pop(0), directions.pop(0))
                self.last_move = self.cur_pos
                continue
            elif cmd == 'm':
                self.cur_pos += Point(directions.pop(0), directions.pop(0))
                self.last_move = self.cur_pos
                continue

            elif cmd in ['L', 'l', 'H', 'h', 'V', 'v']:
                if cmd == 'L':
                    next_pos = Point(directions.pop(0), directions.pop(0))
                elif cmd == 'l':
                    next_pos = self.cur_pos + Point(
                        directions.pop(0),
                        directions.pop(0)
                    )
                elif cmd == 'H':
                    next_pos = Point(directions.pop(0), self.cur_pos.y)
                elif cmd == 'h':
                    next_pos = Point(
                        self.cur_pos.x + Decimal(directions.pop(0)),
                        self.cur_pos.y)
                elif cmd == 'V':
                    next_pos = Point(self.cur_pos.x, directions.pop(0))
                elif cmd == 'v':
                    next_pos = Point(
                        self.cur_pos.x,
                        self.cur_pos.y + Decimal(directions.pop(0)))

                self.segments.append(Line(self.cur_pos, next_pos))
                self.cur_pos = next_pos
            elif cmd in ['C', 'c', 'S', 's']:
                if cmd in ['C', 'c']:
                    control1 = Point(directions.pop(0), directions.pop(0))
                else:
                    try:
                        prev_seg = self.segments[-1]
                        if isinstance(prev_seg, CubicBezier):
                            control1 = self.cur_pos.reflect(prev_seg.control2)
                        else:
                            control1 = self.cur_pos
                    except IndexError:
                        control1 = self.cur_pos

                segment = CubicBezier(
                    self.cur_pos,
                    control1,
                    Point(directions.pop(0), directions.pop(0)),
                    Point(directions.pop(0), directions.pop(0))
                )

                if cmd in ['c', 's']:
                    if cmd == 'c':
                        segment.control1 += self.cur_pos

                    segment.control2 += self.cur_pos
                    segment.end += self.cur_pos

                self.cur_pos = segment.end
                self.segments.append(segment)
            elif cmd in ['Q', 'q', 'T', 't']:
                if cmd in ['Q', 'q']:
                    control = Point(directions.pop(0), directions.pop(0))
                else:
                    try:
                        prev_seg = self.segments[-1]
                        if isinstance(prev_seg, QuadraticBezier):
                            control = self.cur_pos.reflect(prev_seg.control)
                        else:
                            control = self.cur_pos
                    except IndexError:
                        control = self.cur_pos

                segment = QuadraticBezier(
                    self.cur_pos,
                    control,
                    Point(directions.pop(0), directions.pop(0))
                )

                if cmd in ['q', 't']:
                    if cmd == 'q':
                        segment.control += self.cur_pos

                    segment.end += self.cur_pos

                self.cur_pos = segment.end
                self.segments.append(segment)
            elif cmd in ['A', 'a']:
                segment = Arc(
                    self.cur_pos,
                    Point(directions.pop(0), directions.pop(0)),
                    directions.pop(0),
                    bool(int(directions.pop(0))),
                    bool(int(directions.pop(0))),
                    Point(directions.pop(0), directions.pop(0)),
                )

                if cmd == 'a':
                    segment.end += self.cur_pos

                self.cur_pos = segment.end
                self.segments.append(segment)
            elif cmd in ['Z', 'z']:
                segment = Line(
                    self.cur_pos,
                    self.last_move
                )
                self.cur_pos = segment.end
                self.segments.append(segment)
            else:
                raise NotImplementedError(
                    'The direction command `{}` is not implemented'.format(cmd)
                )

    def bbox(self):
        bboxes = [s.bbox() for s in self.segments]
        return (
            Point(min(b[0].x for b in bboxes), min(b[0].y for b in bboxes)),
            Point(max(b[1].x for b in bboxes), max(b[1].y for b in bboxes)),
        )


class PathError(ValueError):
    pass


def extract_path(svg_tree: ElementTree.ElementTree) -> dict:
    path = svg_tree.find('.//{*}path')
    if path is None:
        raise PathError("No 'path' element found in document")

    # TODO: handle missing 'd' attribute
    d = path.attrib['d']
    # TODO: handle invalid 'd' attribute values
    bbox = Path(d).bbox()

    return {
        'd': d,
        'bbox': bbox,
    }


def construct_svg_tree(
    paths: Sequence[dict], previews: bool = False
) -> ElementTree.Element:
    grid_size = math.ceil(math.sqrt(len(paths)))
    preview_elements = []

    # root <svg> element
    root = ElementTree.Element('svg', attrib={
        'xmlns': 'http://www.w3.org/2000/svg',
    })

    if previews:
        root.set('xmlns:xlink', 'http://www.w3.org/1999/xlink')
        root.set('viewBox', '0 0 {0} {0}'.format(grid_size * 100))

    for i, path in enumerate(paths):
        symbol = ElementTree.SubElement(root, 'symbol')
        symbol.set('id', path['name'])
        symbol.set('viewBox', '{0.x} {0.y} {1.x} {1.y}'.format(
            path['bbox'][0],
            path['bbox'][1] - path['bbox'][0]
        ))

        s_path = ElementTree.SubElement(symbol, 'path')
        s_path.set('d', path['d'])

        if previews:
            preview_elements.append(ElementTree.Element('use', attrib={
                'xlink:href': '#{name}'.format(**path),
                'x': str(i % grid_size * 100),
                'y': str(i // grid_size * 100),
                'width': '100',
                'height': '100',
            }))

    # put "preview" elements at the end of the docuemnt
    root.extend(preview_elements)
    return root


def run():
    import argparse
    import sys

    cmd_parser = argparse.ArgumentParser(
        description='Generate SVG spritesheet from multiple .svg files')
    cmd_parser.add_argument('-o', '--output',
                            help='output .svg file (default: STDOUT)')
    cmd_parser.add_argument('-p', '--previews', action='store_true',
                            help='include previews in output SVG')
    cmd_parser.add_argument('svg_file', nargs='+', help='input .svg file')
    args = cmd_parser.parse_args()

    paths = []
    for filename in args.svg_file:
        try:
            svg_tree = ElementTree.parse(filename)
        except FileNotFoundError as e:
            print(e, file=sys.stderr)
            sys.exit(e.errno)
        except ElementTree.ParseError:
            print(f'{filename}: Unable to parse document', file=sys.stderr)
            sys.exit(1)

        try:
            path = extract_path(svg_tree)
            path['name'] = pathlib.Path(filename).stem
            paths.append(path)
        except PathError as e:
            print(f'{filename}: {e}', file=sys.stderr)
            pass

    sprite_svg_tree = construct_svg_tree(paths, args.previews)
    svg_str = ElementTree.tostring(sprite_svg_tree).decode()

    if args.output:
        with open(args.output, 'w') as output:
            print(svg_str, file=output)
    else:
        print(svg_str)


if __name__ == '__main__':
    run()
